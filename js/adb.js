// Copyright 2017 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict'

/**
 * Bind the callbacks to the page!
 */
window.onload = function() {
  const close = document.querySelector('#close');
  close.addEventListener('click', function() {
    window.close();
    // Cancel the default click behavior.
    return false;
  });
};
