// Copyright 2017 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict'

/**
 * When the adb app icon is launched, show the adb page.
 */
chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('html/adb.html', {
    'bounds': {
      'width': 600,
      'height': 250,
      'left': 20,
      'top': 50,
    },
    minWidth: 200,
    minHeight: 100,
  });
});
