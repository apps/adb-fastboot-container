# ADB/Fastboot Chrome App Container

This is the code to create a container for ADB/Fastboot for use in CrOS in a
Chrome App.  This won't work on any other platform.

## Glossary

*   **CRX**: Chrome Extension: Shorthand name for the zip archive.
    Chrome itself will use `.crx` as a zip with all the CWS metadata inserted.
*   **CWS**: Chrome Web Store: Where people upload Chrome apps/extensions.
*   **NaCl**: Native Client: Chrome technology to safely run native code on the
    open web, and a precursor to WASM.  While this project doesn't use NaCl at
    all, the `manifest.json` uses NaCl-specific-paths in conjunction with the
    CWS to shrink downloads sent to users (see the `platforms` key).
*   **OCI**: The Open Container Initiative.  A shorthand name to refer to a
    container image that follows the OCI specification.
*   **WASM**: Web Assembly.  We don't use it in this project.

## Updating

You can use the `./bin/run-all` script to execute all the commands.
See that file too for more details on each sub-step.

## File Layout

Here's a rough layout of the output:

*   `dist/`: All cached/intermediate artifacts are under this.
    Nuke it to restart.
    *   `build/`: Various build roots are in here.
        *   `adb-*/`: The directory tree which creates the crx zip.
        *   `signing/`: Intermediates for creating archives for signing.
        *   `arm/`: The sysroot for the OCI container for the ARM arch.
        *   `x86-64/`: The sysroot for the OCI container for the x86-64 arch.
    *   `cache/`: All download artifacts are cached here.
    *   `crx/`: The final zip files to upload to the CWS.
        This is the "final" extension and everything it needs.
    *   `signing/`: All signing files (before & after signing).
        These are just the OCI container images.
*   `_platform_specific/`: All architecture specific output files live here.
    This is where the final imageloader files are saved.

We duplicate the files under each arch dir to make testing with official images
and developer images (devkey) easier.
That's why you'll see e.g. `_platform_specific/x86-64/adb/` and
`_platform_specific/x86-64/adb-devkeys/`.
At runtime, imageloader will skip files that don't match the registered keys
and move on to the ones that do.
The contents of these directories are the same other than the signed details.

## Local Testing

Once you run `./bin/run-all`, you can take the zip files under `dist/build/crx/`
and copy them to your Chromebook and load them unpacked.
This will only work if your device is running devkeys (*not* the same thing as
just being in dev mode).
This is because the `/usr/share/misc/oci-container-key-pub.der` is baked into
the rootfs.

If you don't want to boot your own build of Chromium OS, you can copy over the
`image.squash` file from `_platform_specific/x86-64/adb/` and unpack it (using
`unsquashfs`) or loopback mount it into a path like `/usr/local/`.
Then run `run_oci` manually on that output directory (see the `cmd_c` code in
`/usr/bin/crosh` for more details).

You probably want to run `start device-jail` before doing so as crosh itself
normally takes care of spawning that.
Note that crosh itself will also stop device-jail once you're done.

You can even use `chroot` to quickly test lower level details in the `rootfs/`
subdir of the squashfs image.
