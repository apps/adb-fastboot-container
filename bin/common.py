#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Common util code."""

from __future__ import print_function

import base64
import errno
import json
import os
import re
import shutil

import requests


TOPDIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DISTDIR = os.path.join(TOPDIR, 'dist')
BUILDDIR = os.path.join(DISTDIR, 'build')
CACHEDIR = os.path.join(DISTDIR, 'cache')


def cmd_str(cmd):
    """Return a string for |cmd| for display."""
    return ' '.join(cmd)


def makedirs(path):
    """Create the |path| directory and don't error if it exists."""
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def remove(path):
    """Remove |path| and don't error if it doesn't exist."""
    try:
        os.remove(path)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise


def rmtree(path):
    """Remove |path| and don't error if it doesn't exist."""
    try:
        shutil.rmtree(path)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise


def get_cached_file(uri, filename=None):
    """Download |uri| and cache it locally."""
    makedirs(CACHEDIR)

    if filename is None:
        filename = re.sub(r'[:/]', '_', uri)

    local_file = os.path.join(CACHEDIR, filename)
    if not os.path.exists(local_file):
        tmp_file = local_file + '.tmp'
        r = requests.get(uri, stream=True)
        if r.status_code == 404:
            return None
        print('Downloading', uri)
        with open(tmp_file, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024 * 1024):
                f.write(chunk)
        os.rename(tmp_file, local_file)

    print('Using cached file', local_file)
    return local_file


def get_gitiles_file(uri, filename=None):
    """Download |uri| from gitiles and decode it."""
    path = get_cached_file(uri + '?format=TEXT', filename=filename)
    with open(path) as f:
        return base64.b64decode(f.read())


def gs_uri_to_https(uri):
    """Convert a gs:// uri to a public https:// one."""
    if uri.startswith('gs://'):
        uri = os.path.join('https://storage.googleapis.com', uri[5:])
    return uri


def read_crx_manifest():
    """Read the manifest.json."""
    return json.load(open(os.path.join(TOPDIR, 'manifest.json')))
